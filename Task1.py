import numpy as np
import CommonTools

data = CommonTools.loadData()

radii = np.zeros(10) # maximum distances for each digit
center_vectors = np.zeros((10, 256)) # center vectors for each digit
for digit in range(0, 9+1):
    index_list = np.where(data["train_indices"] == digit)
    digit_data = data["train_data"][index_list]
    #CommonTools.visualise(digit_data[0])
    center_vector = np.average(digit_data, axis=0)
    center_vectors[digit, :] = center_vector
    distance_matrix = np.sqrt(np.sum((digit_data - center_vector)**2, axis=1)) # broadcasted
    radius = np.max(distance_matrix) # maximum absolute distance
    radii[digit] = radius
    print("Digit:", digit, "- Radius:", radius)

center_pt_distances = np.zeros((10, 10)) # inter-cloud center distances
for i in range(0, 9+1):
    for j in range(i, 9+1): # upper triangle: symmetry
        if (i != j): # i = j --> dist = 0
            center_pt_distances[i, j] = np.sqrt(np.sum((center_vectors[i, :] - center_vectors[j, :])**2))

ind_max = np.where(center_pt_distances == np.amax(center_pt_distances))
ind_min = np.where(center_pt_distances == np.amin(center_pt_distances[np.where(center_pt_distances != 0)]))
print("Minimum center distances (digits):", ind_min[0], ind_min[1])
print("Maximum center distances (digits):", ind_max[0], ind_max[1])

num_contained_points = np.zeros(10) # number of digit data points contained in each cloud
for digit in range(0, 9+1):
    vec = center_vectors[digit, :]
    points_distance_vector = np.sqrt(np.sum((data["train_data"] - vec)**2, axis=1))
    num_contained_points[digit] = np.count_nonzero(points_distance_vector < radii[digit])

print("Contained points for each digit ball:", num_contained_points)
