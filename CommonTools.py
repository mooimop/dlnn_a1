import numpy as np
import os
from PIL import Image

data_directory = os.getcwd() + "\\data"

def loadData():
    test_data = np.genfromtxt(data_directory + "\\" + "test_in.csv", delimiter=',')
    test_indices = np.genfromtxt(data_directory + "\\" + "test_out.csv", delimiter=',')
    train_data = np.genfromtxt(data_directory + "\\" + "train_in.csv", delimiter=',')
    train_indices = np.genfromtxt(data_directory + "\\" + "train_out.csv", delimiter=',')

    return {"test_data" : test_data, "test_indices" : test_indices,
            "train_data" : train_data, "train_indices" : train_indices}

def visualise(digit_vector, dim=16):
    digit_array = (np.reshape(digit_vector, (dim, dim)) + 1) / 2 # renormalise
    white_color = np.array([255, 255, 255])
    color_array = np.tensordot(digit_array, white_color, axes=0).astype(np.uint8)
    img = Image.fromarray(color_array, 'RGB')
    img.show('my.png')

if __name__ == "__main__":
    visualise(np.array([0, -1, -1, -1]), dim=2)
    print(np.array([[1, 2], [2, 4]])**2)
