import numpy as np
import math

class MLP(object):
    def __new__(cls, layer_nodes):
        if len(layer_nodes) < 2:
            print("@MLP.__new__: too few layers, must have at least two layers")
            return None
        else:
            return object.__new__(cls)
        
    def __init__(self, layer_nodes):
        self.num_layers = len(layer_nodes)
        self.layer_nodes = layer_nodes # input --> hidden 1 ... hidden k --> output
        self.node_values = [np.zeros(i + 1) for i in self.layer_nodes] # node values + 1 bias ATTN: the output layer also gets a bias! (last index)
        for i in range(self.num_layers):
            self.node_values[i][self.layer_nodes[i]] = 1 # bias
        self.weight_values = [np.zeros((self.layer_nodes[i] + 1, self.layer_nodes[i + 1])) for i in range(self.num_layers - 1)] # weight matrices, 1 bias
        # weight matrix: index (i, j) is weight from node i to j, i.e. w_{i, j}
        # last column are biases

    def propagate(self, from_layer=0, to_layer='out'):
        to_layer_numeric = [to_layer, self.num_layers][to_layer == 'out']
        for l in range(from_layer + 1, to_layer_numeric): # ordered
            self.node_values[l][0:-1] = np.matmul(self.weight_values[l - 1].T, self.node_values[l - 1]) # exclude bias
        return

    def setLayer(self, index, vec):
        index_numeric = [index, 0][index in ["first", "in", "input"]]
        if (np.shape(self.node_values[index_numeric][0:-1]) == np.shape(vec)):
            self.node_values[index_numeric][0:-1] = vec # exclude bias
            return
        else:
            print("@MLP.setLayer: invalid vector shapes:", np.shape(vec), "where", np.shape(self.node_values[index_numeric][0:-1]), "required")
            return
    
    def getLayer(self, index):
        index_numeric = [index, self.num_layers - 1][index in ["last", "out", "output"]]
        return self.node_values[index_numeric][0:-1] # exclude bias

    def setWeights(self, layer_interval, weight_matrix):
        if np.shape(weight_matrix) == np.shape(self.weight_values[layer_interval]):
            self.weight_values[layer_interval] = weight_matrix # attn: biases not excluded! Biases are in the last column
            return
        else:
            print("@MLP.setWeights: invalid shapes:", np.shape(weight_matrix), "where", np.shape(self.weight_values[layer_interval]), "required")
            return

    def _activator_identity(self, vec):
        return vec

    def _activator_logistic(self, vec):
        return (1 / (1 + math.e**(-vec)))

    def _activator_norm_identity(self, vec):
        return (vec / np.sum(vec))

    def _activator_ReLU(self, vec):
        return (np.maximum(vec, 0))

    def error_SSE(self, y_vec, normalise=False):
        """Calculate SSE of output layer with y_vec as training vector
        normalise (bool): normalise the output layer vector first (mean, sigma)"""
        y_pred = self.node_values[self.num_layers - 1][0:-1]
        if normalise == True:
            norm_output = y_pred - np.average(y_pred) # mu
            n = self.layer_nodes[self.num_layers - 1]
            norm_output = [0, np.sqrt(np.sum(norm_output**2) / n)][n > 0] # sigma
            error = np.sum((norm_output - y_vec)**2)
            return error
        else:
            error = np.sum((y_pred - y_vec)**2)
            return error

if __name__ == "__main__":
    print("hey")
    ml = MLP([256, 512, 1])
    ml.propagate()
    print(ml.getLayer("out"))
    
